import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  log: { clicks: any[], over: any[] } = { over: [], clicks: [] }
  fresh: boolean = false;

  data: Object = {
    "imageUrl": "",
    "action": "",
    "buttonText": ""
  };

  constructor(public http: HttpClient) {
    this.http.get("http://localhost:1111/data").subscribe((result) => {
      this.data = result;
    })
    setInterval(() => {
      if (this.fresh) {
        this.fresh = false;
        this.http.post("http://localhost:2222/send", this.log).subscribe(
          (ok) => { },
          (err) => { console.error("error", err) }
        )
        this.log = { over: [], clicks: [] };
      }
    }, 500);
  }

  over(ev) {
    this.fresh = true;
    this.log.over.push(this.extractInfo(ev));
  }

  click(ev) {
    this.fresh = true;
    this.log.clicks.push(this.extractInfo(ev));
  }

  extractInfo(ev) {
    let evInfo = {
      x: ev.x,
      y: ev.y,
      type: ev.type,
      date: new Date()
    };
    return evInfo;
  }

}