const express = require('express')
const app = express()
const port = 7070
const cors = require('cors')

app.use(cors())

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/home.html');
});

app.listen(port, (err) => {
	if (err) {
		return console.log('something bad happened', err)
	}
	console.log(`The server is listening on ${port}`)
})