This is the resolution to a fullstack JS excercise.

# Frontend:
* Go into `front/my-app` and run `npm i` and then `npm start` to run the app.

# Backend:
* Run `npm i` in the root folder
* Run `npm start all` to start the node services

You can access this by opening
	localhost:7070