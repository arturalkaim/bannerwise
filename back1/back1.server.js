const express = require('express')
const app = express()
const port = 1111
const cors = require('cors')

app.use(cors())

app.get('/data', (req, res) => {
	response = {
		"imageUrl": "https://via.placeholder.com/350x150",
		"action": "https://bannerwise.io/",
		"buttonText": "Click Me"
	};
	res.send(JSON.stringify(response))
})

app.listen(port, (err) => {
	if (err) {
		return console.log('something bad happened', err)
	}

	console.log(`The server is listening on ${port}`)
})