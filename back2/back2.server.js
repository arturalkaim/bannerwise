const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const port = 2222
const cors = require('cors')

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));


app.post('/*', (req, res) => {
	console.log(req.body); //TODO: save this info
	res.end();
})

app.listen(port, (err) => {
	if (err) {
		return console.log('something bad happened', err)
	}
	console.log(`The server is listening on ${port}`)
})